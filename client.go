package client

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
)

// API is single ABRA API endpoint
type API struct {
	baseURL  string
	username string
	password string
	app      string
}

// NewAPI creates new API
func NewAPI(baseURL, username, password, app string) *API {
	return &API{
		baseURL:  baseURL,
		username: username,
		password: password,
		app:      app,
	}
}

// QueryObjects queries business objects
func (api *API) QueryObjects(q *Query) (*http.Request, error) {
	b, err := json.Marshal(q)
	if err != nil {
		return nil, err
	}
	r := bytes.NewReader(b)
	return api.newRequest(http.MethodPost, "/query", nil, r)
}

// AddObject creates new business object
func (api *API) AddObject(class string, obj interface{}, q *Query) (*http.Request, error) {
	b, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	r := bytes.NewReader(b)
	v := q.Values()
	return api.newRequest(http.MethodPost, "/"+url.PathEscape(class), v, r)
}

func (api *API) newRequest(method string, url string, v url.Values, b io.Reader) (*http.Request, error) {
	q := v.Encode()
	if q != "" {
		q = "?" + q
	}
	url = api.baseURL + url + q
	req, err := http.NewRequest(method, url, b)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(api.username, api.password)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	return req, nil
}
