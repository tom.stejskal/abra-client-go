package client

import (
	"bytes"
	"encoding/json"
	"strings"
	"testing"
)

func TestField(t *testing.T) {
	q := NewQuery()
	q.Field("foo")
	testMarshalJSON(t, q, `{"select":["foo"]}`)
}

func TestFieldAs(t *testing.T) {
	q := NewQuery()
	q.FieldAs("foo", "bar")
	testMarshalJSON(t, q, `{"select":[{"name":"bar","value":"foo"}]}`)
}

func TestQuery(t *testing.T) {
	q := NewQuery()
	u := NewQuery()
	u.Field("bar")
	q.Query("foo", u)
	testMarshalJSON(t, q, `{"select":[{"name":"foo","value":{"select":["bar"]}}]}`)
}

func TestQueryAs(t *testing.T) {
	q := NewQuery()
	u := NewQuery()
	u.Field("bar")
	q.QueryAs("foo", "baz", u)
	testMarshalJSON(t, q,
		`{"select":[{"name":"baz","value":{"field":"foo","query":{"select":["bar"]}}}]}`)
}

func TestKey(t *testing.T) {
	q := NewQuery()
	q.Key("foo")
	testMarshalJSON(t, q, `{"orderby":["foo"]}`)
}

func TestKeyDesc(t *testing.T) {
	q := NewQuery()
	q.KeyDesc("foo")
	testMarshalJSON(t, q, `{"orderby":[{"desc":true,"value":"foo"}]}`)
}

func TestValues(t *testing.T) {
	q := NewQuery()
	q.Class("foo")
	e := "class=foo"
	a := q.Values().Encode()
	checkString(t, e, a)

	q = NewQuery()
	q.Field("foo")
	q.Field("bar")
	e = "select=foo%2Cbar"
	a = q.Values().Encode()
	checkString(t, e, a)

	q = NewQuery()
	q.Take(10)
	e = "take=10"
	a = q.Values().Encode()
	checkString(t, e, a)

	q = NewQuery()
	q.Skip(10)
	e = "skip=10"
	a = q.Values().Encode()
	checkString(t, e, a)

	q = NewQuery()
	q.Where("foo > 0")
	e = "where=foo+%3E+0"
	a = q.Values().Encode()
	checkString(t, e, a)

	q = NewQuery()
	q.Key("foo")
	q.KeyDesc("bar")
	e = "orderby=foo%2Cbar+desc"
	a = q.Values().Encode()
	checkString(t, e, a)
}

func checkString(t *testing.T, e, a string) {
	if strings.Compare(e, a) != 0 {
		t.Errorf("expected %v, got %v", e, a)
	}
}

func TestMarshalQueryToJSON(t *testing.T) {
	q := NewQuery()
	e := "{}"
	testMarshalJSON(t, q, e)

	q = NewQuery()
	q.Field("foo")
	e = `{"select":["foo"]}`
	testMarshalJSON(t, q, e)

	q = NewQuery()
	q.Class("bar")
	e = `{"class":"bar"}`
	testMarshalJSON(t, q, e)

	q = NewQuery()
	q.Take(10)
	e = `{"take":10}`
	testMarshalJSON(t, q, e)

	q = NewQuery()
	q.Skip(10)
	e = `{"skip":10}`
	testMarshalJSON(t, q, e)

	q = NewQuery()
	q.Where("foo > 0")
	e = `{"where":"foo \u003e 0"}`
	testMarshalJSON(t, q, e)

	q = NewQuery()
	q.Key("foo")
	e = `{"orderby":["foo"]}`
	testMarshalJSON(t, q, e)
}

func TestMarshalFieldToJSON(t *testing.T) {
	f := &field{expr: "foo"}
	e := `"foo"`
	testMarshalJSON(t, f, e)

	f = &field{expr: "foo", alias: "bar"}
	e = `{"name":"bar","value":"foo"}`
	testMarshalJSON(t, f, e)

	q := NewQuery()
	q.Field("baz")
	f = &field{expr: "foo", query: q}
	e = `{"name":"foo","value":{"select":["baz"]}}`
	testMarshalJSON(t, f, e)

	f = &field{expr: "foo", alias: "bar", query: q}
	e = `{"name":"bar","value":{"field":"foo","query":{"select":["baz"]}}}`
	testMarshalJSON(t, f, e)
}

func TestMarshalKeyToJSON(t *testing.T) {
	k := &key{name: "foo"}
	e := `"foo"`
	testMarshalJSON(t, k, e)

	k = &key{name: "foo", desc: true}
	e = `{"desc":true,"value":"foo"}`
	testMarshalJSON(t, k, e)
}

func testMarshalJSON(t *testing.T, v interface{}, e string) {
	a, err := json.Marshal(v)
	if err != nil {
		t.Error(err)
	}
	if bytes.Compare([]byte(e), a) != 0 {
		t.Errorf("expected %s, got %s", e, a)
	}
}
