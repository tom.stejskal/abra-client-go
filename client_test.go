package client

import (
	"testing"
)

func TestQueryObjects(t *testing.T) {
	api := newAPI()
	q := NewQuery()
	q.Field("foo")
	_, err := api.QueryObjects(q)
	if err != nil {
		t.Error(err)
	}
	_, err = api.QueryObjects(nil)
	if err != nil {
		t.Error(err)
	}
}

func TestAddObject(t *testing.T) {
	api := newAPI()
	o := make(map[string]interface{})
	o["bar"] = "baz"
	q := NewQuery()
	q.Field("id")
	_, err := api.AddObject("foo", o, q)
	if err != nil {
		t.Error(err)
	}
	_, err = api.AddObject("foo", o, nil)
	if err != nil {
		t.Error(err)
	}
}

func newAPI() *API {
	return NewAPI("https://localhost", "foo", "bar", "")
}
