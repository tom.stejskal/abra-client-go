package client

import (
	"encoding/json"
	"net/url"
	"strconv"
	"strings"
)

// Query to the ABRA API
type Query struct {
	class  string
	fields []*field
	take   int
	skip   int
	where  string
	keys   []*key
}

type field struct {
	expr  string
	alias string
	query *Query
}

type key struct {
	name string
	desc bool
}

// NewQuery create new query
func NewQuery() *Query {
	return &Query{}
}

// Class sets the class
func (q *Query) Class(class string) {
	q.class = class
}

// Field adds field
func (q *Query) Field(expr string) {
	q.fields = append(q.fields, &field{expr: expr})
}

// FieldAs adds field with alias
func (q *Query) FieldAs(expr, alias string) {
	q.fields = append(q.fields, &field{expr: expr, alias: alias})
}

// Query adds field with sub-query
func (q *Query) Query(expr string, query *Query) {
	q.fields = append(q.fields, &field{expr: expr, query: query})
}

// QueryAs adds field with sub-query and alias
func (q *Query) QueryAs(expr, alias string, query *Query) {
	q.fields = append(q.fields, &field{expr: expr, alias: alias, query: query})
}

// Take sets how many records to take
func (q *Query) Take(take int) {
	q.take = take
}

// Skip sets how many records to skip
func (q *Query) Skip(skip int) {
	q.skip = skip
}

// Where sets condition
func (q *Query) Where(where string) {
	q.where = where
}

// Key adds sorting key
func (q *Query) Key(name string) {
	q.keys = append(q.keys, &key{name: name})
}

// KeyDesc add sorting key in descending order
func (q *Query) KeyDesc(name string) {
	q.keys = append(q.keys, &key{name: name, desc: true})
}

// Values creates URL query values
// TODO: nested queries
// TODO: aliases
func (q *Query) Values() url.Values {
	v := url.Values{}
	if q == nil {
		return v
	}
	if q.class != "" {
		v.Add("class", q.class)
	}
	if len(q.fields) > 0 {
		s := make([]string, 0, len(q.fields))
		for _, f := range q.fields {
			if f.query == nil {
				s = append(s, f.expr)
			}
		}
		if len(s) > 0 {
			v.Add("select", strings.Join(s, ","))
		}
	}
	if q.take > 0 {
		v.Add("take", strconv.Itoa(q.take))
	}
	if q.skip > 0 {
		v.Add("skip", strconv.Itoa(q.skip))
	}
	if q.where != "" {
		v.Add("where", q.where)
	}
	if len(q.keys) > 0 {
		s := make([]string, 0, len(q.keys))
		for _, k := range q.keys {
			n := k.name
			if k.desc {
				n += " desc"
			}
			s = append(s, n)
		}
		v.Add("orderby", strings.Join(s, ","))
	}
	return v
}

// MarshalJSON is method to write query to JSON
func (q *Query) MarshalJSON() ([]byte, error) {
	v := make(map[string]interface{})
	if q.class != "" {
		v["class"] = q.class
	}
	if len(q.fields) > 0 {
		v["select"] = q.fields
	}
	if q.take > 0 {
		v["take"] = q.take
	}
	if q.skip > 0 {
		v["skip"] = q.skip
	}
	if q.where != "" {
		v["where"] = q.where
	}
	if len(q.keys) > 0 {
		v["orderby"] = q.keys
	}
	return json.Marshal(v)
}

// MarshalJSON is method to write field to JSON
func (f *field) MarshalJSON() ([]byte, error) {
	if f.alias != "" || f.query != nil {
		v := make(map[string]interface{})
		if f.query == nil {
			v["name"] = f.alias
			v["value"] = f.expr
		} else {
			if f.alias == "" {
				v["name"] = f.expr
				v["value"] = f.query
			} else {
				v["name"] = f.alias
				u := make(map[string]interface{})
				u["field"] = f.expr
				u["query"] = f.query
				v["value"] = u
			}
		}
		return json.Marshal(v)
	}
	return json.Marshal(f.expr)
}

// MarshalJSON is method to write key to JSON
func (k *key) MarshalJSON() ([]byte, error) {
	if k.desc {
		v := make(map[string]interface{})
		v["value"] = k.name
		v["desc"] = k.desc
		return json.Marshal(v)
	}
	return json.Marshal(k.name)
}
